require 'gem2deb/rake/testtask'

task :test do
  Dir.chdir('test')
end

Gem2Deb::Rake::TestTask.new(:test) do |t|
  t.libs << '../lib'
  t.libs << '../test'
  t.test_files = FileList['*_test.rb']
  t.verbose = false
end

task :default => :test
